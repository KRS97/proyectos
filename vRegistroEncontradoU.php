<!DOCTYPE html>
<html>
<head>
	<?php echo view('vEnlaceHead');?>
	<title>Registro Encontrado</title>
</head>
<body>
	<?php echo view('nVavbar');?>
	<div class="container">
		<div class="row">
			<h1>Registros encontrados</h1>
		<form method="POST" action="../Home/actualizarRegistro">
			<input type="hidden" class="form-control" id="id_usuario" name="id_usuario" value="<?php echo $id_usuario; ?>">
			<div class="mb-3 row">
				<label for="email" class="col-sm-2 col-form-label">nombre</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $nombre; ?>">
				</div>
			</div>
			<div class="mb-3 row">
				<label for="email" class="col-sm-2 col-form-label">apellido</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="apellido" name="apellido" value="<?php echo $apellido; ?>">
				</div>
			</div>
			<div class="mb-3 row">
				<label for="email" class="col-sm-2 col-form-label">correo</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" id="correo" name="correo" value="<?php echo $correo; ?>">
				</div>
			</div>
			<div class="mb-3 row">
				<label for="password" class="col-sm-2 col-form-label">Password</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="password" name="password" value="<?php echo $password; ?>">
				</div>
			</div>
			<div class="mb-3 row">
				<label for="email" class="col-sm-2 col-form-label">Direccion</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="direccion" name="direccion" value="<?php echo $direccion; ?>">
				</div>
			</div>
			<div class="mb-3 row">
				<label for="email" class="col-sm-2 col-form-label">Ciudad</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="ciudad" name="ciudad" value="<?php echo $ciudad; ?>">
				</div>
			</div>
			<div class="mb-3 row">
				<button type="submit" class="btn btn-primary mb-3">Actualizar</button>
			</div>
			
		</form>
</body>
    <?php echo view('vFooter');?>
</html>