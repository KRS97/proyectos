<!DOCTYPE html>
<html>
<head>
	<?php echo view('vEnlaceHead');?>
	<title>MostrarRegistro</title>
</head>
<body>
	<?php echo view('nVavbar');?>
	<div class="container">
		<div class="row">
			<h1>Registros actuales</h1>
			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col">ID</th>
						<th scope="col">Nombre</th>
						<th scope="col">Apellido</th>
						<th scope="col">Correo</th>
						<th scope="col">Password</th>
						<th scope="col">Direccion</th>
						<th scope="col">Ciudad</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					
					$db =\Config\Database::connect();
					$query = $db->query("SELECT * FROM registro");
					foreach ($query->getResult('array') as $usuario) { ?>
						<tr>
							<td><?php echo $usuario['id_usuario']; ?></td>
							<td><?php echo $usuario['nombre'];     ?></td>
							<td><?php echo $usuario['apellido'];   ?></td>
							<td><?php echo $usuario['correo'];     ?></td>
							<td><?php echo $usuario['password'];   ?></td>
							<td><?php echo $usuario['direccion'];  ?></td>
							<td><?php echo $usuario['ciudad'];     ?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</body>
    <?php echo view('vFooter');?>
</html>