<!DOCTYPE html>
<html>
<head>
	<?php echo view('vEnlaceHead');?>
	<title>Registro Encontrado</title>
</head>
<body>
	<?php echo view('nVavbar');?>
	<div class="container">
		<div class="row">
			<h1>Registros encontrados</h1>
		<form method="POST" action="../Home/actualizarRegistro">
			<input type="hidden" class="form-control" id="id_usuario" name="id_usuario" value="<?php echo $id_usuario; ?>">
			<div class="mb-3 row">
				<label for="email" class="col-sm-2 col-form-label">nombre</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $nombre; ?>">
				</div>
			</div>
			<div class="mb-3 row">
				<label for="email" class="col-sm-2 col-form-label">talla</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="talla" name="talla" value="<?php echo $talla; ?>">
				</div>
			</div>
			<div class="mb-3 row">
				<label for="email" class="col-sm-2 col-form-label">Precio</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" id="precio" name="precio" value="<?php echo $precio; ?>">
				</div>
			</div>
			
			<div class="mb-3 row">
				<label for="email" class="col-sm-2 col-form-label">Departamento</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="departamento" name="departamento" value="<?php echo $departamento; ?>">
				</div>
			<div class="mb-3 row">
				<button type="submit" class="btn btn-primary mb-3">Actualizar</button>
			</div>
			
		</form>
</body>
    <?php echo view('vFooter');?>
</html>