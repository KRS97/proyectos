<!DOCTYPE html>
<html>
<head>
	<?php echo view('vEnlaceHead');?>
	<title>Registro Encontrado</title>
</head>
<body>
	<?php echo view('vVavbar');?>
	<div class="container">
		<div class="row">
			<h1>Registros encontrados</h1>
		<form method="POST" action="../Home/actualizarRegistroP">

			<input type="hidden" class="form-control" id="id_prenda" name="id_prenda" value="<?php echo $id_prenda; ?>">
			<div class="mb-3 row">
				<label for="nombre" class="col-sm-2 col-form-label">nombre</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $nombre; ?>">
				</div>
			</div>
			<div class="mb-3 row">
				<label for="talla" class="col-sm-2 col-form-label">talla</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="talla" name="talla" value="<?php echo $talla; ?> ">
				</div>
			</div>
			<div class="mb-3 row">
				<label for="precio" class="col-sm-2 col-form-label">Precio</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="precio" name="precio" value="<?php echo $precio; ?>">
				</div>
			</div>
			
			<div class="mb-3 row">
				<label for="departamento" class="col-sm-2 col-form-label">Departamento</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="departamento" name="departamento" value="<?php echo $departamento; ?>">
				</div>
			<div class="mb-3 row">
				<button type="submit" class="btn btn-primary mb-3">Actualizar</button>
				<a type="button" class="btn btn-danger mb-3" href="<?php echo base_url(); ?>/Home/eliminarRegistroP/<?php echo $id_prenda; ?>">Eliminar</a>
			</div>
			
		</form>
</body>
    <?php echo view('vFooter');?>
</html>