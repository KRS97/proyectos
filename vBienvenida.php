<!DOCTYPE html>
<html>
<head>
	<?php echo view('vEnlaceHead');?>
	<title>Bienvenida</title>
	<style type="text/css">
		.contenedor{
			position: relative;
            display: inline-block;
            text-align: center;
        }
        .texto-encima{
        	position: absolute;
        }
        .centrado{
        	position: absolute;
        	top: 50%;
        	left: 50%;
        	transform: translate(-50%, -50%);
        }
	</style>
</head>
<body>
	<?php echo view('nVavbar');?>
	<div class="container">
		<div class="row">
			<div class="contenedor">
				<img src="https://img2.freepng.es/20180314/ifq/kisspng-clothing-clothes-shop-shopping-stock-photography-children-s-models-model-taiwan-5aa968d5e53d24.396754741521051861939.jpg" alt="Bienvenida" width="1300" height="600">
				<div class="texto-encima"></div>
				<font size="40" face="Lucida Handwriting"><div class="centrado">Bienvenidos</div></font>
			</div>
		</div>
		
	</div>
</body>
     <?php echo view('vFooter');?>

</html>
