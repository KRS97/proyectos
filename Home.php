<?php

namespace App\Controllers;
use App\Models\mUsuarios;
use App\Models\mUsuario;
use App\Models\mIniciar;
use App\Models\mPrendas;
class Home extends BaseController
{
	public function index()
	{
		return view('welcome_message');
	}
	public function bienvenida()
	{
		return view('vBienvenida');
	}
	public function iniciar()
	{
		return view('vIniciar');
	}
	public function usuario()
	{
		return view('vUsuario');
	}
	public function prendas(){
		return view('vPrendas');
	}
	/*INICIO DE FUNCIONES PARA INSERTAR*/
	public function RUsuario()
	{
		$mUsuarios = new mUsuarios();
		$usuarioNuevo=[
			"nombre"    => $_POST['nombre'],
			"apellido"  => $_POST['apellido'],
			"correo"    => $_POST['correo'],
			"password"  => $_POST['password'],
			"direccion" => $_POST['direccion'],
			"ciudad"    => $_POST['ciudad']
		];
		$mUsuarios->insert($usuarioNuevo);
		$datoId['idRegistrado']= $mUsuarios->db->insertID();

		return view("vSuccess", $datoId);
		//print_r($_POST);
	}

	public function RIniciar(){
		$mIniciar = new mIniciar();
		$iniciarSesion=[
			"correo"    => $_POST['correo'],
			"password"  => $_POST['password']
		];
		$mIniciar->insert($iniciarSesion);
		$datoId['idRegistrado']= $mIniciar->db->insertID();

		return view("vSuccess", $datoId);
	}
	public function RPrendas()
	{
		$mPrendas = new mPrendas();
		$prendaNueva=[
			"nombre"    => $_POST['nombre'],
			"talla"  => $_POST['talla'],
			"precio"    => $_POST['precio'],
			"departamento"  => $_POST['departamento']
		];
		$mPrendas->insert($prendaNueva);
		$datoId['idRegistrado']= $mPrendas->db->insertID();

		return view("vSuccess", $datoId);
	}
	/*INICIO DE FUNCIONES PARA MOSTRAR*/
	public function mrUsuarios(){
		$mUsuarios = new mUsuarios();
		$todos=$mUsuarios->findAll();
		$usuarios=array('usuarios'=>$todos);

		return view("vRegistro",$usuarios);
	}
	public function mrInicio(){
		$mIniciar = new mIniciar();
		$todos=$mIniciar->findAll();
		$usuarios=array('usuarios'=>$todos);

		return view('vmInicio',$usuarios);
	}
	public function mrPrendas(){
		$mPrendas = new mPrendas();
		$todos=$mPrendas->findAll();
		$prendas=array('prendas'=>$todos);

		return view('vRegistrosP',$prendas);
	}
	/*INICIO DE FUNCIONES PARA BUSCAR*/
	public function buscarRegistroU(){
		$mUsuarios = new mUsuarios();
		$id_usuario = $_POST['id_usuario'];
		$usuario=$mUsuarios->find($id_usuario);
		return view("vRegistroEncontrado",$usuario);
		//$todos=mUsuarios->where('correo','Miguel@mail.com')->finAll();
		//$usuarios=array('usuarios' =>$todos );
		//return view("vRegistro",$usuarios);
	}
	public function buscarRegistroP(){
		$mPrendas = new mPrendas();
		$id_prenda = $_POST["id_prenda"];
		$prendas=$mPrendas->find($id_prenda);
		return view("vRegistroEncontradoP",$prendas);
		
	}
	/*INICIO DE FUNCIONES PARA ACTUALIZar*/
	public function actualizarRegistro(){
		$mUsuarios= new mUsuarios();
		$id_usuario = $_POST['id_usuario'];
		$usuarioActualizado=[
			"nombre"=>$_POST['nombre'],
			"apellido"=>$_POST['apellido'],
		    "correo" => $_POST['correo'],
		    "password" => $_POST['password'],
		    "direccion"=>$_POST['direccion'],
		    "ciudad"=>$_POST['ciudad']
		];
		$mUsuarios->update($id_usuario, $usuarioActualizado);
		//$usuario=$mUsuarios->find($id_usuario);
		return $this->mostrarRegistros();
	}
	public function actualizarRegistroP(){
		$mPrendas= new mPrendas();
		$id_prenda = $_POST['id_prenda'];
		$prendaActualizado=[
			"nombre"=>$_POST['nombre'],
			"talla"=>$_POST['talla'],
		    "precio" => $_POST['precio'],
		    "departamento" => $_POST['departamento']
		  	];
		$mPrendas->update($id_prenda, $prendaActualizado);
		//$usuario=$mUsuarios->find($id_usuario);
		return $this->mrPrendas();
	}
	public funtion eleminarRegistro($id){
		$mUsuarios = new mUsuarios();
		$id_usuario = $id;
		$mUsuarios->delete($id_usuario);

		return $this->mrUsuarios();
	}
	public funtion eleminarRegistroI($id){
		$mIniciar = new mIniciar();
		$id_u = $id;
		$mIniciar->delete($id_u);

		return $this->mrIniciar();
	}
	public function eliminarRegistroP($id)
	{
		$mPrendas = new mPrendas();
		$id_prenda =$id;
		$mPrendas->delete($id_prenda);

		return $this->mrPrendas();
	}
	

	
}
