<!DOCTYPE html>
<html>
<head>
	<?php echo view('vEnlaceHead');?>
	<title>Exito</title>
</head>
<body>
	<?php echo view('nVavbar');?>
	<!--
	<div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
		<div class="toast-header">
			<img src="..." class="rounded me-2" alt="...">
			<strong class="me-auto">Registro</strong>
			<small>Hace 1 segundo</small>
			<button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
    	    <p>El usuario se registro con exito, su identificador es el: <?php echo $idRegistrado; ?>
        </div>
    </div>
-->
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="alert alert-info" role="alert">
				<p>El usuario se registro con exito, su identificador es el: <?php echo $idRegistrado; ?></p>
			</div>
		</div>
	</div>
	<!--
	-->
	
</body>
    <?php echo view('vFooter');?>
</html>