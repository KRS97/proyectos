<!DOCTYPE html>
<html>
<head>
	<?php echo view('vEnlacesHead'); ?>
	<title>Registros</title>
</head>
<body>
	<?php echo view('vNavBar'); ?>
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-md-auto">
				<h1 class="alert alert-primary" role="alert">Registros actuales</h1>
			</div>
		<div class="row">
			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col">ID</th>
						<th scope="col">Nombre</th>
						<th scope="col">Talla</th>
						<th scope="col">Precio</th>
						<th scope="col">Departamento</th>


					</tr>
				</thead>
				<tbody>
					<?php foreach ($prendas as $prendas) { ?>
					<tr>
						<td><?php echo $prendas['id_prenda']; ?></td>
						<td><?php echo $prendas['nombre']; ?></td>
						<td><?php echo $prendas['talla']; ?></td>
						<td><?php echo $prendas['precio']; ?></td>
						<td><?php echo $prendas['departamento']; ?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</body>
<?php echo view('vFooter'); ?>
</html>